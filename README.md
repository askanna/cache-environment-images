# Cache environment images

In AskAnna we cache environment images. This decreases the run time of a job. On a weekly base we update frequently
used cached images via this project. If a new version of an environment image is available, we will update to the
latest version.

In the file [askanna.yml](askanna.yml) you can add additional jobs to cache other images. You can do so by making a
merge request in this project.

[Read more about Environmnet in AskAnna](https://docs.askanna.io/environments/)

import os
import askanna

project_suuuid = os.getenv("AA_PROJECT_SUUID")
job_name = os.getenv("AA_JOB_NAME")
jobs = askanna.job.list(project_suuuid)

for job in jobs:
    if not job.name == job_name:
        askanna.run.start(job_suuid=job.short_uuid)
